from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_delete
from django.utils import timezone
# Create your models here.

class ActivationKey(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	key = models.CharField(max_length=100)


class Member(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE) 
	active = models.BooleanField(default=False)
	last_active = models.DateTimeField(blank=True, null=True, default=timezone.now)
	user_key = models.CharField(max_length=100, null=True, blank=True)

	@receiver(post_delete, sender='web.Member')
	def increment_roomtype(instance, **kwargs):
		instance.user.delete()
	
	def __str__(self):
		return self.user.username


class InviteMember(models.Model):
	sender = models.ForeignKey(Member, related_name='user_sender', on_delete=models.CASCADE)
	receiver = models.ForeignKey(Member, related_name='user_receiver', on_delete=models.CASCADE)
	date = models.DateTimeField(default=timezone.now)
	is_accept = models.BooleanField(default=False)


class CallHistory(models.Model):
    dailer = models.ForeignKey(Member, related_name='caller_id', blank=True, null=True, on_delete=models.CASCADE)
    receiver = models.ForeignKey(Member, related_name='receiver_id', blank=True, null=True, on_delete=models.CASCADE)
    duration = models.TimeField(default=0 )
    call_on_date = models.DateTimeField(blank=True, null=True, default=timezone.now)
