from django.conf.urls import url, include
from django.urls import path
from web.views import *
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', Dashboard.as_view(), name='dashboard'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('verify_user_key/<slug:user_key>', VerifyUserKeyView.as_view(), name='verify-user'),
]