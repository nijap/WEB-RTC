from django.core.mail import EmailMessage
from django.conf import settings

def send_invitation(self, email, key):
	print("Key : ", key)
	url = self.request.build_absolute_uri()
	body = 'You are invited to join an application. Please click the link to join : '
	body +=  url+'verify_user_key/'+str(key)
	subject = 'Invitation'
	email = EmailMessage(subject, body, settings.DEFAULT_FROM_EMAIL, (email,))
	email.content_subtype = 'html'
	
	try:
		email.send()
	except Exception as e:
		return  False
	return True
