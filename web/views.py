from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic.base import TemplateView
from django.contrib import messages
# Create your views here.
from django.contrib.auth.forms import AuthenticationForm
from django.views.generic import FormView, RedirectView, View, CreateView
from web.models import *
from web.forms import UserLoginForm, InviteUserForm
from web.utils import send_invitation
from django.contrib.auth import  authenticate, login, logout
import uuid


class Dashboard(TemplateView):
    template_name = 'dashboard.html'
    form_class = InviteUserForm

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        context['test'] = 'user test'
        context['members'] = Member.objects.filter(active=True)
        context['form'] = InviteUserForm(self.request.POST or None)
        return context
    
    def post(self, request, *args, **kwargs):
        context = self.get_context_data()
        form = context['form']
        
        if form.is_valid():
            email = form.cleaned_data['email']
            password = str(email.split('@')[0])+'@123'
            print("password : ", password)
            new_user, created = User.objects.get_or_create(username=email,
                    email=email,
                    password=password)
            new_user.is_active = False
            new_user.save()

            key = uuid.uuid4().hex[:10].upper()            
            while True:
                if ActivationKey.objects.filter(key=key).exists():
                    key = uuid.uuid4().hex[:6].upper()
                else:
                    break
                    
            ActivationKey.objects.create(user=new_user, key=key)
            mail_status = send_invitation(self, email, key)
            if mail_status:
                messages.success(request, 'Succesfully sent mail')                
            else:
                messages.error(request, 'Couldint sent mmail.')

        return HttpResponseRedirect('/')


class LoginView(FormView):
    template_name = 'login.html'
    form_class = UserLoginForm
    success_url = '.'

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)
        context = {}

        if user:
            if user.is_active:
                login(self.request, user)
                return HttpResponseRedirect('/')
            else:
                context['form'] = form
                context['error'] = 'Inactive'
                return render(self.request, self.template_name, context)
        else:
            context['form'] = form
            context['error'] = 'Invalid credentials'
            return render(self.request, self.template_name, context)


class LogoutView(View):
	def get(self, request):
		logout(request)
		return HttpResponseRedirect('/')


class VerifyUserKeyView(View):

    def get(self, request, *args, **kwargs):
        import pdb; pdb.set_trace()
        user_key = self.kwargs.get('user_key', '')
        print("Key : ", user_key)
        if ActivationKey.objects.filter(key=user_key).exists():
            activation_obj = ActivationKey.objects.get(key=user_key)
            new_user = activation_obj.user
            new_user.is_active = True
            new_user.save()
            member = Member.objects.create(user=new_user)

            return HttpResponseRedirect('/')
        return HttpResponse("wrong key")